<?php
/**
 * Created by PhpStorm.
 * User: lenin
 * Date: 2/23/16
 * Time: 4:26 PM
 */

namespace app\components;

use Imagine\Gd;
use Imagine\Image\Box;
use Yii;

/* @var $file \yii\web\UploadedFile */

/* @var $imagine \Imagine\Gd\Imagine */
class Uploader
{
    public static $originPath = 'original';
    public static $sizes = [
        'large' => 800,
        'medium' => 640,
        'small' => 300,
        'thumb' => 150,
        'low' => 32,
    ];

    /**
     * @param $file
     * @return bool|string
     */
    public static function processTeamLogo($file)
    {
        // validate
        if (Utils::validateFile($file, 'image')) {

            $uploadsDir = 'uploads';
            $imageUploadPath = $uploadsDir . DIRECTORY_SEPARATOR;
            Utils::checkDir($imageUploadPath);

            // save original images directory
            $filename = Utils::getRandomName() . '.' . $file->extension;
            if ($file->saveAs($imageUploadPath . $filename)) {
                // save in images directory
                $size = 150;
                $imagine = new Gd\Imagine();
                $image = $imagine->open($imageUploadPath . $filename);
                $subPath = $imageUploadPath . DIRECTORY_SEPARATOR . 'team' . DIRECTORY_SEPARATOR;
                Utils::checkDir($subPath);
                $image->thumbnail(new Box($size, $size))
                    ->save($subPath . $filename, ['quality' => 90]);
                return $filename;
            }
        }
        return false;
    }

    public static function processTeamMember($file)
    {
        // validate
        if (Utils::validateFile($file, 'image')) {

            $uploadsDir = 'uploads';
            $imageUploadPath = $uploadsDir . DIRECTORY_SEPARATOR. 'member' . DIRECTORY_SEPARATOR;
            Utils::checkDir($imageUploadPath);

            // save original images directory
            $filename = Utils::getRandomName() . '.' . $file->extension;

            if ($file->saveAs($imageUploadPath . $filename)) {
                $size = 150;
                $imagine = new Gd\Imagine();
                $image = $imagine->open($imageUploadPath . $filename);
                $image->thumbnail(new Box($size, $size))->save($imageUploadPath . $filename, ['quality' => 90]);
                return $filename;
            }
        }
        return false;
    }


    public static function uploadCDN($filename, $path, $cdnDir = '')
    {
        $s3 = Yii::$app->get('s3');

        $result = $s3->upload(empty($cdnDir) ? $filename : $cdnDir . '/' . $filename, $path . $filename);
        $awsCDNLinks = $result['ObjectURL'];

        return $awsCDNLinks;
    }

    public static function deleteCDN($filename)
    {
        $s3 = Yii::$app->get('s3');
        $result = $s3->delete($filename);
        return $result;
    }


    // TODO add a file uploader too
} 