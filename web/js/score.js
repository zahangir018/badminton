/**
 * Created by Zahangir on 21/01/2020.
 */

$(function () {

    $('#scorecard-fixtureid').change(function () {
        var fixtureId = $(this).val();
        if (fixtureId) {
            getTeams(fixtureId);
        }
    });

    function getTeams(fixtureId) {
        $.ajax({
            url: ajaxUrl,
            type: 'get',
            data: {
                fixtureId: fixtureId,
            },
            dataType: 'json',
            success: function (data) {
                $('#scorecard-winingteam').children('option').remove();
                $('#scorecard-opponent').children('option').remove();
                $('#scorecard-winingteam').append(data);
                $('#scorecard-opponent').append(data);
            },
            error: function () {
                alert('Error happend!');
            }
        });
    }

    $('#scorecard-winingtype').change(function () {
        var type = $(this).val();
        if (type == '2-0') {
            $('#scorecard-point').val(10);
        } else {
            $('#scorecard-point').val(8);
        }
    });


});