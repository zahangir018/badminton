<?php

namespace app\controllers;

use app\models\Fixture;
use app\models\PointTable;
use app\models\Team;
use Yii;
use app\models\ScoreCard;
use app\models\ScoreCardSearch;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ScoreCardController implements the CRUD actions for ScoreCard model.
 */
class ScoreCardController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                //'only' => ['login', 'logout', 'signup'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'create', 'update', 'delete', 'view'],
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        if (Yii::$app->user->identity->username != 'admin') {
            return $this->redirect('/site/index');
        }
        return parent::beforeAction($action); // TODO: Change the autogenerated stub
    }

    /**
     * Lists all ScoreCard models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ScoreCardSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $teams = \app\models\Team::find()->all();
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'teams' => $teams,
        ]);
    }

    /**
     * Displays a single ScoreCard model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ScoreCard model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ScoreCard();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                $pointTable = PointTable::findOne(['teamId' => $model->winingTeam]);
                $pointTable->totalPoint = $pointTable->totalPoint + $model->point;
                $pointTable->save();
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        $fixtures = Fixture::find()->with(['teamOneDetail', 'teamTwoDetail'])->all();
        return $this->render('create', [
            'model' => $model,
            'fixtures' => ArrayHelper::map($fixtures, 'id', function ($item) {
                return 'Group ' . $item->groupName . ' - ' . $item->category . ' - (' . $item->teamOneDetail->name . ' VS ' . $item->teamTwoDetail->name . ')';
            }),
        ]);
    }

    /**
     * Updates an existing ScoreCard model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $oldModel = $model;

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                if ($oldModel->winingTeam == $model->winingTeam) {
                    $pointTable = PointTable::findOne(['teamId' => $model->winingTeam]);
                    if ($oldModel->point != $model->point) {
                        $pointDifference = $oldModel->point - $model->point;
                        $pointTable->totalPoint = ($pointDifference > 0) ? ($pointTable->totalPoint - 2) : ($pointTable->totalPoint + 2);
                    }
                    $pointTable->save();
                } else {
                    $oldPointTable = PointTable::findOne(['teamId' => $oldModel->winingTeam]);
                    $pointTable = PointTable::findOne(['teamId' => $model->winingTeam]);
                    $oldPointTable->totalPoint = $oldPointTable->totalPoint - $oldModel->point;
                    $pointTable->totalPoint = $pointTable->totalPoint + $model->point;
                    $pointTable->save();
                    $oldPointTable->save();
                }
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        $fixtures = Fixture::find()->with(['teamOneDetail', 'teamTwoDetail'])->all();
        return $this->render('update', [
            'model' => $model,
            'fixtures' => ArrayHelper::map($fixtures, 'id', function ($item) {
                return 'Group ' . $item->groupName . ' - ' . $item->category . ' - (' . $item->teamOneDetail->name . ' VS ' . $item->teamTwoDetail->name . ')';
            }),
            'teams' => ArrayHelper::map(Team::find()->all(), 'id', 'name')
        ]);
    }

    /**
     * Deletes an existing ScoreCard model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        //$this->findModel($id)->delete();
        Yii::$app->session->setFlash('error', 'Delete is not allowed');
        return $this->redirect(['index']);
    }

    /**
     * Finds the ScoreCard model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ScoreCard the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ScoreCard::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }


    public function actionGetTeams($fixtureId)
    {
        $fixture = Fixture::findOne(['id' => $fixtureId]);
        $teams = Team::find()->where(['id' => [$fixture->teamOne, $fixture->teamTwo]])->all();
        $options = '<option>Select team ...</option>';

        foreach ($teams as $key => $value) {
            $options .= '<option value="' . $value->id . '">' . $value->name . '</option>';
        }

        return Json::encode($options);
    }
}
