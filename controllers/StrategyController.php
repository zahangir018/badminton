<?php

namespace app\controllers;

use app\models\Fixture;
use Codeception\Util\Fixtures;
use Yii;
use app\models\Strategy;
use app\models\StrategySearch;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * StrategyController implements the CRUD actions for Strategy model.
 */
class StrategyController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                //'only' => ['login', 'logout', 'signup'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['logout', 'index', 'create', 'update', 'delete', 'view'],
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Strategy models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new StrategySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Strategy model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Strategy model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Strategy();
        if (Yii::$app->request->isPost) {

            $post = Yii::$app->request->post();
            foreach ($post['Strategy']['playerId'] as $item) {
                $st = new Strategy();
                $st->fixtureId = $post['Strategy']['fixtureId'];
                $st->teamId = (int)Yii::$app->user->identity->team;
                $st->playerId = $item;
                $st->save();
            }
            return $this->redirect('index');
        }


        return $this->render('create', [
            'model' => $model,
            'fixtures' => ArrayHelper::map(Fixture::find()->with(['teamOneDetail', 'teamTwoDetail'])->where(['teamOne' => (int)Yii::$app->user->identity->team])->orWhere(['teamTwo' => (int)Yii::$app->user->identity->team])->all(), 'id', function ($item) {
                return 'Group ' . $item->groupName . ' - ' . $item->category . ' - (' . $item->teamOneDetail->name . ' VS ' . $item->teamTwoDetail->name . ')';
            }),
        ]);
    }

    /**
     * Updates an existing Strategy model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
            'fixtures' => ArrayHelper::map(Fixture::find()->with(['teamOneDetail', 'teamTwoDetail'])->all(), 'id', function ($item) {
                return 'Group ' . $item->groupName . ' - ' . $item->category . ' - (' . $item->teamOneDetail->name . ' VS ' . $item->teamTwoDetail->name . ')';
            }),
        ]);
    }

    /**
     * Deletes an existing Strategy model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Strategy model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Strategy the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Strategy::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist . ');
    }
}
