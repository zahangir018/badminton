-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 21, 2020 at 12:02 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.1.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

--
-- Database: `badminton`
--

-- --------------------------------------------------------

--
-- Table structure for table `point_table`
--

CREATE TABLE `point_table` (
  `id` int(11) NOT NULL,
  `teamId` int(11) NOT NULL,
  `totalPoint` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `point_table`
--

INSERT INTO `point_table` (`id`, `teamId`, `totalPoint`) VALUES
(1, 3, 8);

-- --------------------------------------------------------

--
-- Table structure for table `score_card`
--

CREATE TABLE `score_card` (
  `id` int(11) NOT NULL,
  `fixtureId` int(11) NOT NULL,
  `winingTeam` int(11) NOT NULL,
  `opponent` int(11) NOT NULL,
  `winingType` enum('2-0','2-1') NOT NULL,
  `point` int(11) NOT NULL,
  `pointDetails` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `score_card`
--

INSERT INTO `score_card` (`id`, `fixtureId`, `winingTeam`, `opponent`, `winingType`, `point`, `pointDetails`) VALUES
(1, 1, 2, 1, '2-0', 10, 'test'),
(2, 3, 3, 2, '2-1', 8, 'test');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `point_table`
--
ALTER TABLE `point_table`
  ADD PRIMARY KEY (`id`),
  ADD KEY `teamId` (`teamId`);

--
-- Indexes for table `score_card`
--
ALTER TABLE `score_card`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk-score-card-fixtureId` (`fixtureId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `point_table`
--
ALTER TABLE `point_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `score_card`
--
ALTER TABLE `score_card`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `point_table`
--
ALTER TABLE `point_table`
  ADD CONSTRAINT `fk-point-table-teamId` FOREIGN KEY (`teamId`) REFERENCES `team` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `score_card`
--
ALTER TABLE `score_card`
  ADD CONSTRAINT `fk-score-card-fixtureId` FOREIGN KEY (`fixtureId`) REFERENCES `fixtures` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;
