<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PointTable */

$this->title = Yii::t('app', 'Create Point Table');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Point Tables'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="point-table-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
