<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PointTable */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="point-table-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'teamId')->textInput() ?>

    <?= $form->field($model, 'totalPoint')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
