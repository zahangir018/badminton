<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ScoreCard */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Score Cards'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="score-card-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'fixture',
                'value' => function ($model) {
                    return 'Group '.$model->fixture->groupName.' - '.$model->fixture->category.' - ('.$model->winner->name.' VS '. $model->looser->name.')';
                },
            ],
            [
                'attribute' => 'winingTeam',
                'value' => function ($model) {
                    return $model->winner->name;
                },
            ],
            [
                'attribute' => 'opponent',
                'value' => function ($model) {
                    return $model->looser->name;
                },
            ],
            [
                'attribute' => 'winingType',
                'value' => function ($model) {
                    return \app\models\ScoreCard::WINNING_TYPE[$model->winingType];
                },
            ],
            'point',
            'pointDetails:ntext',
        ],
    ]) ?>

</div>
