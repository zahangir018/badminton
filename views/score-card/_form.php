<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ScoreCard */
/* @var $form yii\widgets\ActiveForm */

$this->registerJs(
    "var ajaxUrl = '" . Yii::$app->request->baseUrl . '/score-card/get-teams' . "'; var _csrf='" . Yii::$app->request->getCsrfToken() . "';",
    \yii\web\View::POS_HEAD,
    'url'
);

$this->registerJsFile(
    '@web/js/score.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]
);
?>

<div class="score-card-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'fixtureId')->dropDownList($fixtures, ['prompt' => '']) ?>

    <?= $form->field($model, 'winingTeam')->dropDownList(($model->isNewRecord) ? [] : $teams, ['prompt' => '']) ?>

    <?= $form->field($model, 'opponent')->dropDownList(($model->isNewRecord) ? [] : $teams, ['prompt' => '']) ?>

    <?= $form->field($model, 'winingType')->dropDownList(\app\models\ScoreCard::WINNING_TYPE, ['prompt' => '']) ?>

    <?= $form->field($model, 'point')->textInput(['readOnly' => true]) ?>

    <?= $form->field($model, 'pointDetails')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
