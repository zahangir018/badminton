<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ScoreCardSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="score-card-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'fixtureId') ?>

    <?= $form->field($model, 'winingTeam') ?>

    <?= $form->field($model, 'opponent') ?>

    <?= $form->field($model, 'winingType') ?>

    <?php // echo $form->field($model, 'point') ?>

    <?php // echo $form->field($model, 'pointDetails') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
