<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ScoreCard */

$this->title = Yii::t('app', 'Create Score Card');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Score Cards'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="score-card-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'fixtures' => $fixtures,
    ]) ?>

</div>
