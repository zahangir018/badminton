<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ScoreCardSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Score Cards');
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="score-card-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create Score Card'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            //'id',
            [
                'attribute' => 'fixture',
                'value' => function ($model) {
                    return 'Group '.$model->fixture->groupName.' - '.$model->fixture->category.' - ('.$model->winner->name.' VS '. $model->looser->name.')';
                },
            ],
            [
                'attribute' => 'winingTeam',
                'value' => function ($model) {
                    return $model->winner->name;
                },
                'filter' => \yii\helpers\ArrayHelper::map($teams, 'id', 'name')
            ],
            [
                'attribute' => 'opponent',
                'value' => function ($model) {
                    return $model->looser->name;
                },
                'filter' => \yii\helpers\ArrayHelper::map($teams, 'id', 'name')
            ],
            [
                'attribute' => 'winingType',
                'value' => function ($model) {
                    return \app\models\ScoreCard::WINNING_TYPE[$model->winingType];
                },
                'filter' => \app\models\ScoreCard::WINNING_TYPE
            ],
            'point',
            'pointDetails:ntext',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
