<?php

/* @var $this yii\web\View */

$this->title = 'Fixtures';
?>
<div class="site-index">
    <div class="body-content">
        <div class="row">
            <h1><?= \yii\helpers\Html::encode($this->title) ?></h1>
            <?php \yii\widgets\Pjax::begin(); ?>
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

            <?= \yii\grid\GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    'groupName',
                    [
                        'attribute' => 'teamOne',
                        'label' => 'Team One',
                        'value' => function ($model) {
                            return (!empty($model->teamOneDetail)) ? $model->teamOneDetail->name : '';
                        },
                        'filter' => \yii\helpers\ArrayHelper::map(\app\models\Team::find()->all(), 'id', 'name')
                    ],
                    [
                        'attribute' => 'teamTwo',
                        'label' => 'Team Two',
                        'value' => function ($model) {
                            return (!empty($model->teamTwoDetail)) ? $model->teamTwoDetail->name : '';
                        },
                        'filter' => \yii\helpers\ArrayHelper::map(\app\models\Team::find()->all(), 'id', 'name')
                    ],
                    'category',
                    'gameTime:datetime',
                ],
            ]); ?>

            <?php \yii\widgets\Pjax::end(); ?>
        </div>
    </div>
</div>
