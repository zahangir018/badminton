<?php

/* @var $this yii\web\View */

$this->title = 'Scores';
?>
<div class="site-index">

    <div class="body-content">

        <div class="row">
            <h1><?= \yii\helpers\Html::encode($this->title) ?></h1>
            <?php \yii\widgets\Pjax::begin(); ?>
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

            <?= \yii\grid\GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    //'id',
                    [
                        'attribute' => 'fixture',
                        'value' => function ($model) {
                            return 'Group '.$model->fixture->groupName.' - '.$model->fixture->category.' - ('.$model->winner->name.' VS '. $model->looser->name.')';
                        },
                    ],
                    [
                        'attribute' => 'winingTeam',
                        'value' => function ($model) {
                            return $model->winner->name;
                        },
                        'filter' => \yii\helpers\ArrayHelper::map($teams, 'id', 'name')
                    ],
                    [
                        'attribute' => 'opponent',
                        'value' => function ($model) {
                            return $model->looser->name;
                        },
                        'filter' => \yii\helpers\ArrayHelper::map($teams, 'id', 'name')
                    ],
                    [
                        'attribute' => 'winingType',
                        'value' => function ($model) {
                            return \app\models\ScoreCard::WINNING_TYPE[$model->winingType];
                        },
                        'filter' => \app\models\ScoreCard::WINNING_TYPE
                    ],
                    'point',
                    'pointDetails:ntext',
                ],
            ]); ?>

            <?php \yii\widgets\Pjax::end(); ?>
        </div>
    </div>
</div>
