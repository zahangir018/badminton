<?php

/* @var $this yii\web\View */

$this->title = 'SHARETRIP BADMINTON TOURNAMENT 2020';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Teams!</h1>
    </div>

    <div class="body-content">

        <div class="row">
            <?php foreach ($teams as $team): ?>

                <div class="col-sm-6 col-md-4">
                    <div class="thumbnail">
                        <img class="img-rounded" src="<?= \yii\helpers\Url::to('/uploads/team/') . $team->logo ?>"
                             alt="">
                        <div class="caption">
                            <h3><?= $team->name ?></h3>
                            <p><small><?= $team->owner ?></small></p>
                            <p><?= $team->tagline ?></p>
                            <p>
                                <a href="#" class="btn btn-default" role="button">Won:<span
                                            class="badge">42</span></a>
                                <a href="#" class="btn btn-default" role="button">Lost:<span
                                            class="badge">42</span></a>
                                <a href="#" class="btn btn-default" role="button">Point:<span
                                            class="badge"><?= $team->point->totalPoint ?></span></a></p>
                        </div>
                    </div>
                </div>

            <?php endforeach; ?>

        </div>

    </div>
</div>
