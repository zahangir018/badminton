<?php

/* @var $this yii\web\View */

$this->title = 'Point Table';
?>
<div class="site-index">

    <div class="body-content">

        <div class="row">
            <h1><?= \yii\helpers\Html::encode($this->title) ?></h1>

            <?php \yii\widgets\Pjax::begin(); ?>
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

            <?= \yii\grid\GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    //'id',
                    ['attribute' => 'team',
                        'value' => function ($model) {
                            return $model->team->name;
                        }
                    ]                   ,
                    'totalPoint',
                ],
            ]); ?>

            <?php \yii\widgets\Pjax::end(); ?>
        </div>
    </div>
</div>
