<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\TeamSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Teams');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="team-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create Team'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            'tagline',
            'owner',
            //'logo',
            [
                'attribute' => 'logo',
                'format' => 'html',
                'value' => function ($model) {
                    return Html::img($model->imageurl('logo', 'team'), ['width' => '60']);
                },
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update} {delete}',
                'contentOptions' => [ 'style' => 'width: 100px;' ],
                'buttons'=>[
                    'view' => function ($url, $model, $key) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', ['view', 'id' => $key], [
                            'title' => Yii::t('app', 'Details'),
                            'class' => 'btn btn-success btn-xs custom_button',
                            'data-pjax' => '0',
                        ]);
                    },
                    'update' => function ($url, $model, $key) {

                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['update', 'id' => $key], [
                            'title' => Yii::t('app', 'Edit'),
                            'class' => 'btn btn-primary btn-xs custom_button',
                            'data-pjax' => '0',
                        ]);
                    },
                    'delete' => function ($url, $model, $key) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', ['delete', 'id' => $key],
                            [
                                'title' => Yii::t('app', 'Delete'),
                                'data-ajax' => '0',
                                'data-method' => 'POST',
                                'data-confirm' => 'Are you sure you want to delete this item?',
                                'class' => 'btn btn-danger btn-xs custom_button',
                            ]);
                    }
                ],
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
