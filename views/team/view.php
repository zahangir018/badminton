<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Team */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Teams'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="team-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Add Team Members'), ['team-member/create', 'teamId' => $model->id], ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'tagline',
            'owner',
            [
                'attribute' => 'logo',
                'format' => 'html',
                'value' => function ($model) {
                    return Html::img($model->imageurl('logo', 'team'), ['width' => '60']);
                },
            ],
        ],
    ]) ?>

    <h2>Members</h2>
    <table class="table table-bordered table-responsive table-hover">
        <thead class="bg-primary">
        <tr>
            <th>#</th>
            <th>Name</th>
            <th>Rank</th>
            <th>Thobra</th>
        </tr>
        </thead>
        <tbody>
        <?php
        foreach ($model->members as $key => $member) {
            ?>
            <tr>
                <td><?= ($key + 1) ?></td>
                <td><?= $member->name ?></td>
                <td><?= $member->rank ?></td>
                <td><?=   Html::img($member->imageurl('image', 'member'), ['width' => '100']); ?></td>
            </tr>
            <?php
        }
        ?>

        </tbody>
    </table>

</div>
