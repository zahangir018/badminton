<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\FixtureSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Fixture';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fixtures-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Fixture', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'groupName',
            [
                'attribute' => 'teamOne',
                'label' => 'Team One',
                'value' => function ($model) {
                    return (!empty($model->teamOneDetail)) ? $model->teamOneDetail->name : '';
                },
                'filter' => \yii\helpers\ArrayHelper::map(\app\models\Team::find()->all(), 'id', 'name')
            ],
            [
                'attribute' => 'teamTwo',
                'label' => 'Team Two',
                'value' => function ($model) {
                    return (!empty($model->teamTwoDetail)) ? $model->teamTwoDetail->name : '';
                },
                'filter' => \yii\helpers\ArrayHelper::map(\app\models\Team::find()->all(), 'id', 'name')
            ],
            'category',
            'gameTime:datetime',
            //'createdAt',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
