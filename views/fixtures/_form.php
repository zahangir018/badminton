<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\datetime\DateTimePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Fixture */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="fixtures-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'groupName')->dropDownList(['A' => 'A', 'B' => 'B'], ['prompt' => '']) ?>

    <?= $form->field($model, 'teamOne')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\Team::find()->asArray()->all(), 'id', 'name'), ['prompt' => '']) ?>

    <?= $form->field($model, 'teamTwo')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\Team::find()->asArray()->all(), 'id', 'name'), ['prompt' => '']) ?>

    <?= $form->field($model, 'category')->dropDownList(\app\models\Fixture::CATEGORY, ['prompt' => '']) ?>

    <?= $form->field($model, 'gameTime')->widget(DateTimePicker::classname(), [
        'name' => 'datetime_10',
        'options' => ['placeholder' => 'Select Game Time'],
        //'convertFormat' => true,
        'pluginOptions' => [
            'format' => 'yyyy-mm-dd h:i:s',
            'todayHighlight' => true,
            'autoclose' => true
        ]
    ])->label('Time');
    ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
