<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Fixture */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Fixture', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="fixtures-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'groupName',
            [
                'attribute' => 'teamOneDetail',
                'label' => 'Team One',
                'value' => function ($model) {
                    return (!empty($model->teamOneDetail)) ? $model->teamOneDetail->name : '';
                }
            ],
            [
                'attribute' => 'teamTwoDetail',
                'label' => 'Team Two',
                'value' => function ($model) {
                    return (!empty($model->teamTwoDetail)) ? $model->teamTwoDetail->name : '';
                }
            ],
            'category',
            'gameTime:datetime',
        ],
    ]) ?>

</div>
