<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\StrategySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Strategies';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="strategy-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Strategy', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'fixtureId',
                'label' => 'Fixture',
                'value' => function ($model) {
                    return 'Group ' . $model->fixtureDetail->groupName . ' - ' . $model->fixtureDetail->category . ' - (' . $model->fixtureDetail->teamOneDetail->name . ' VS ' . $model->fixtureDetail->teamTwoDetail->name . ')';
                }
            ],
            [
                'attribute' => 'teamId',
                'label' => 'Team',
                'value' => function ($model) {
                    return $model->teamDetail->name;
                }
            ],
            [
                'attribute' => 'playerId',
                'label' => 'Player',
                'value' => function ($model) {
                    return $model->player->name;
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
