<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Strategy */

$this->title = 'Update Strategy: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Strategies', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="strategy-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'fixtures' => $fixtures,
    ]) ?>

</div>
