<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \app\models\TeamMember;
use \yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Strategy */
/* @var $form yii\widgets\ActiveForm */
$teamMembers = [];
if (!empty(Yii::$app->user->identity->team)) {
    $teamMembers = ArrayHelper::map(TeamMember::findAll(['teamId' => (int)Yii::$app->user->identity->team]), 'id', 'name');
}

?>

<div class="strategy-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'fixtureId')->dropDownList($fixtures, ['prompt' => '']) ?>

    <?= $form->field($model, 'playerId')->dropDownList($teamMembers, ['prompt' => '', 'multiple' => true, 'required' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
