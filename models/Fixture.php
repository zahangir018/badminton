<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "fixtures".
 *
 * @property int $id
 * @property string|null $groupName
 * @property int $teamOne
 * @property int $teamTwo
 * @property string $category
 * @property string $gameTime
 * @property string $createdAt
 */
class Fixture extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'fixtures';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['teamOne', 'teamTwo', 'gameTime', 'category'], 'required'],
            [['teamOne', 'teamTwo'], 'integer'],
            [['gameTime', 'createdAt'], 'safe'],
            [['groupName'], 'string', 'max' => 255],
        ];
    }


    const CATEGORY = [
        "First Male Double" => "First Male Double",
        "Second Male Double" => "Second Male Double",
        "First Male Single" => "First Male Single",
        "Second Male Single" => "Second Male Single",
        "Female Single" => "Female Single"
    ];


    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'groupName' => 'Group Name',
            'teamOne' => 'Team One',
            'teamTwo' => 'Team Two',
            'category' => 'Category',
            'gameTime' => 'Game Time',
            'createdAt' => 'Created At',
        ];
    }

    public function getTeamOneDetail()
    {
        return $this->hasOne(Team::className(), ['id' => 'teamOne']);
    }

    public function getTeamTwoDetail()
    {
        return $this->hasOne(Team::className(), ['id' => 'teamTwo']);
    }
}
