<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PointTable;

/**
 * PointTableSearch represents the model behind the search form of `app\models\PointTable`.
 */
class PointTableSearch extends PointTable
{
    public $team;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'teamId', 'totalPoint'], 'integer'],
            [['team'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PointTable::find();

        // add conditions that should always apply here
        $query->with(['team']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['team'] = [
            'asc' => ['team.name' => SORT_ASC],
            'desc' => ['team.name' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'teamId' => $this->teamId,
            'totalPoint' => $this->totalPoint,
        ]);

        $query->andFilterWhere(['like', 'team.name', $this->team]);

        return $dataProvider;
    }
}
