<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ScoreCard;

/**
 * ScoreCardSearch represents the model behind the search form of `app\models\ScoreCard`.
 */
class ScoreCardSearch extends ScoreCard
{
    public $fixture;
    public $winner;
    public $looser;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'fixtureId', 'winingTeam', 'opponent', 'point'], 'integer'],
            [['winingType', 'pointDetails', 'winner', 'looser'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ScoreCard::find();

        // add conditions that should always apply here
        $query->with(['fixture', 'winner', 'looser']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['fixture'] = [
            'asc' => ['fixtures.name' => SORT_ASC],
            'desc' => ['fixtures.name' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'fixtureId' => $this->fixtureId,
            'winingTeam' => $this->winingTeam,
            'opponent' => $this->opponent,
            'point' => $this->point,
        ]);

        $query->andFilterWhere(['like', 'winingType', $this->winingType])
            ->andFilterWhere(['like', 'winingTeam', $this->winingTeam])
            ->andFilterWhere(['like', 'opponent', $this->opponent])
            ->andFilterWhere(['like', 'fixtures.groupName', $this->fixture])
            ->orFilterWhere(['like', 'fixtures.category', $this->fixture])
            ->andFilterWhere(['like', 'pointDetails', $this->pointDetails]);

        return $dataProvider;
    }
}
