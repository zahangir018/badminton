<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%team}}".
 *
 * @property int $id
 * @property string $name
 * @property string $tagline
 * @property string $owner
 * @property string $logo
 */
class Team extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%team}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'tagline', 'owner', 'logo'], 'required'],
            [['name', 'tagline', 'logo'], 'string', 'max' => 255],
            [['owner'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'tagline' => 'Tagline',
            'owner' => 'Owner',
            'logo' => 'Logo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMembers()
    {
        return $this->hasMany(TeamMember::className(), ['teamId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPoint()
    {
        return $this->hasOne(PointTable::className(), ['teamId' => 'id']);
    }

    public function imageurl($logo, $dir)
    {
        return Yii::$app->request->BaseUrl . '/uploads/' . $dir . '/' . $this->$logo;
    }
}
