<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "strategy".
 *
 * @property int $id
 * @property int $fixtureId
 * @property int $teamId
 * @property int $playerId
 */
class Strategy extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'strategy';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fixtureId', 'teamId', 'playerId'], 'required'],
            [['fixtureId', 'teamId', 'playerId'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fixtureId' => 'Fixture',
            'teamId' => 'Team',
            'playerId' => 'Player',
        ];
    }

    public function getFixtureDetail()
    {
        return $this->hasOne(Fixture::className(), ['id' => 'fixtureId']);
    }

    public function getTeamDetail()
    {
        return $this->hasOne(Team::className(), ['id' => 'teamId']);
    }

    public function getPlayer()
    {
        return $this->hasOne(TeamMember::className(), ['id' => 'playerId']);
    }
}
