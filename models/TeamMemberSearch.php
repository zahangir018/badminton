<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TeamMember;

/**
 * TeamMemberSearch represents the model behind the search form of `app\models\TeamMember`.
 */
class TeamMemberSearch extends TeamMember
{
    public $team;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'teamId', 'rank'], 'integer'],
            [['name', 'image', 'team'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TeamMember::find();

        // add conditions that should always apply here
        $query->joinWith(['team']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['team'] = [
            // The tables are the ones our relation are configured to
            // in my case they are prefixed with "tbl_"
            'asc' => ['team.name' => SORT_ASC],
            'desc' => ['team.name' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'teamId' => $this->teamId,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'team.name', $this->team])
            ->andFilterWhere(['like', 'image', $this->image]);

        return $dataProvider;
    }
}
