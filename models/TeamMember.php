<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%team_member}}".
 *
 * @property int $id
 * @property int $teamId
 * @property string $name
 * @property string $image
 *
 * @property Team $team
 */
class TeamMember extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%team_member}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['teamId', 'name', 'image'], 'required'],
            [['teamId', 'rank'], 'integer'],
            [['name', 'image'], 'string', 'max' => 255],
            [['teamId'], 'exist', 'skipOnError' => true, 'targetClass' => Team::className(), 'targetAttribute' => ['teamId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'teamId' => Yii::t('app', 'Team'),
            'name' => Yii::t('app', 'Name'),
            'rank' => Yii::t('app', 'Rank'),
            'image' => Yii::t('app', 'Image'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeam()
    {
        return $this->hasOne(Team::className(), ['id' => 'teamId']);
    }

    public function imageurl($logo, $dir)
    {
        return Yii::$app->request->BaseUrl . '/uploads/' . $dir . '/' . $this->$logo;
    }
}
