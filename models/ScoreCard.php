<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%score_card}}".
 *
 * @property int $id
 * @property int $fixtureId
 * @property int $winingTeam
 * @property int $opponent
 * @property string $winingType
 * @property int $point
 * @property string|null $pointDetails
 *
 * @property Fixture $fixture
 */
class ScoreCard extends \yii\db\ActiveRecord
{
    const WINNING_TYPE = ['2-0' => 'With Bonus (2 - 0)', '2-1' => 'Without Bonus(2 - 1)'];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%score_card}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fixtureId', 'winingTeam', 'opponent', 'winingType', 'point'], 'required'],
            [['fixtureId', 'winingTeam', 'opponent', 'point'], 'integer'],
            [['winingType', 'pointDetails'], 'string'],
            [['fixtureId'], 'exist', 'skipOnError' => true, 'targetClass' => Fixture::className(), 'targetAttribute' => ['fixtureId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'fixtureId' => Yii::t('app', 'Fixture'),
            'winingTeam' => Yii::t('app', 'Wining Team'),
            'opponent' => Yii::t('app', 'Opponent'),
            'winingType' => Yii::t('app', 'Wining Type'),
            'point' => Yii::t('app', 'Point'),
            'pointDetails' => Yii::t('app', 'Point Details'),
        ];
    }

    /**
     * Gets query for [[Fixture]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getFixture()
    {
        return $this->hasOne(Fixture::className(), ['id' => 'fixtureId']);
    }

    public function getWinner()
    {
        return $this->hasOne(Team::className(), ['id' => 'winingTeam']);
    }

    public function getLooser()
    {
        return $this->hasOne(Team::className(), ['id' => 'opponent']);
    }
}
