<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Strategy;

/**
 * StrategySearch represents the model behind the search form of `app\models\Strategy`.
 */
class StrategySearch extends Strategy
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'fixtureId', 'teamId', 'playerId'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Strategy::find()->where(['teamId' => \Yii::$app->user->identity->team]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'fixtureId' => $this->fixtureId,
            'teamId' => $this->teamId,
            'playerId' => $this->playerId,
        ]);

        return $dataProvider;
    }
}
