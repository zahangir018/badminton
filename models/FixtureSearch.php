<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Fixture;

/**
 * FixtureSearch represents the model behind the search form of `app\models\Fixture`.
 */
class FixtureSearch extends Fixture
{
    public $teamOneDetail;
    public $teamTwoDetail;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'teamOne', 'teamTwo'], 'integer'],
            [['groupName', 'gameTime', 'createdAt', 'teamOneDetail', 'teamTwoDetail', 'category'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Fixture::find();

        // add conditions that should always apply here
        $query->with(['teamOneDetail', 'teamTwoDetail']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['teamOneDetail'] = [
            // The tables are the ones our relation are configured to
            // in my case they are prefixed with "tbl_"
            'asc' => ['team.name' => SORT_ASC],
            'desc' => ['team.name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['teamTwoDetail'] = [
            // The tables are the ones our relation are configured to
            // in my case they are prefixed with "tbl_"
            'asc' => ['team.name' => SORT_ASC],
            'desc' => ['team.name' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'teamOne' => $this->teamOne,
            'teamTwo' => $this->teamTwo,
            'gameTime' => $this->gameTime,
            'createdAt' => $this->createdAt,
        ]);

        $query->andFilterWhere(['like', 'groupName', $this->groupName])
            ->andFilterWhere(['like', 'teamOne', $this->teamOne])
            ->andFilterWhere(['like', 'teamTwo', $this->teamTwo])
            ->andFilterWhere(['like', 'category', $this->category]);

        return $dataProvider;
    }
}
