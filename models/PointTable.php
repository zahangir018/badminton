<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%point_table}}".
 *
 * @property int $id
 * @property int $teamId
 * @property int $totalPoint
 *
 * @property Team $team
 */
class PointTable extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%point_table}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['teamId', 'totalPoint'], 'required'],
            [['teamId', 'totalPoint'], 'integer'],
            [['teamId'], 'unique'],
            [['teamId'], 'exist', 'skipOnError' => true, 'targetClass' => Team::className(), 'targetAttribute' => ['teamId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'teamId' => Yii::t('app', 'Team'),
            'totalPoint' => Yii::t('app', 'Total Point'),
        ];
    }

    /**
     * Gets query for [[Team]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTeam()
    {
        return $this->hasOne(Team::className(), ['id' => 'teamId']);
    }
}
