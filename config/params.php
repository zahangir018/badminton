<?php

return [
    'adminEmail' => 'ask@tb-bd.com',

    // upload settings
    'uploadOptions' => [
        //'path' => 'uploads', set as bootstrap in common/config/bootstrap.php
        'maxFileSize' => 1024 * 1024 * 10, // 10 MB
        'maxFileCount' => 10, // at a time 10 file can be uploaded,
        'allowedFileTypes' => [
            'pdf' => 'application/pdf',
            'doc' => 'application/doc',
            'csv' => 'application/csv',
        ],
        'allowedImageTypes' => [
            'jpg' => 'image/jpeg',
            'jpeg' => 'image/jpeg',
            'jpe' => 'image/jpeg',
            'gif' => 'image/gif',
            'png' => 'image/png',
        ],
    ],
    'basicInfo' => [
        'address' => 'Tour Booking Bangladesh Ltd.<br>Road 13/C House 52 Block E<br> House#45, Road#13/C, Block#E, Banani Dhaka-121',
        'hotline' => '8809617617617,8801709642304, 8801709642308 & 8801709642307',
        'email' => 'ask@tb-bd.com',
        'androidAppURL' => 'https://play.google.com/store/apps/details?id=com.ice9apps.tbbd',
        'iosAppURL' => 'https://itunes.apple.com/ca/app/tbbd/id1232576597?mt=8'
    ],
];
